@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
			<h2>Company List</h2>
            <div class="card">
                <!--<div class="card-header"></div>-->
				 <div class="card-header">
					<a href="{{route('company.create')}}" class="btn btn-success">+ Add  New</a>
				</div>

                <div class="card-body">
				
					@if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif
				
					 <table class="table">
						<thead>
						  <tr>
							<th>id</th>
							<th>Logo</th>
							<th>Name</th>
							<th>Email</th>
							<th>Website</th>
							<th>Action</th>
						  </tr>
						</thead>
						<tbody>
						 @if(!empty($companyData))
							@foreach($companyData as $row)
								<tr>
									<td scope="row">{{$row->id}}</th>
									<td>
										@if(strlen($row->logo) >= 3)
											<img src="{{url('/images/'.$row->logo)}}" height="65px" />
										@endif
									</td>
									<td>{{ucfirst($row->name)}}</td>
									<td>{{$row->email}}</td>
									<td>{{$row->website}}</td>
									<td>
										<a href="{{route('company.edit', ['id' => $row->id])}}" class="btn btn-info">Edit</a> 
										<a href="{{route('company.delete', ['id' => $row->id])}}" class="btn btn-danger" 	onclick="return confirm('Do you really want to delete this record?')">Delete</a>
									</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					  </table>
					  
					  {{ $companyData->links('vendor.pagination.custom') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
