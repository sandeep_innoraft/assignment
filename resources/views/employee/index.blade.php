@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
			<h2>Employee List</h2>
            <div class="card">
				
                <div class="card-header">
					<a href="{{route('employee.create')}}" class="btn btn-success">+ Add  New</a>
				</div>

                <div class="card-body">
				
					@if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif
				
					 <table class="table">
						<thead>
						  <tr>
							<th>id</th>
							<th>Company</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Action</th>
						  </tr>
						</thead>
						<tbody>
						 @if(!empty($employeeData))
							@foreach($employeeData as $row)
								<tr>
									<td scope="row">{{$row->id}}</th>
									<td>{{ucfirst($row->company->name)}}</td>
									<td>{{ucfirst($row->first_name)}}</td>
									<td>{{$row->last_name}}</td>
									<td>{{$row->phone}}</td>
									<td>{{$row->email}}</td>
									<td>
										<a href="{{route('employee.edit', ['id' => $row->id])}}" class="btn btn-info">Edit</a> 
										<a href="{{route('employee.delete', ['id' => $row->id])}}" class="btn btn-danger" 	onclick="return confirm('Do you really want to delete this record?')">Delete</a>
									</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					  </table>
				
					{{ $employeeData->links('vendor.pagination.custom') }}
					<!--pagination-->
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
