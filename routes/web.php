<?php
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
});
 */
Auth::routes();
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('/', [AuthController::class, 'index'])->name('login');
Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('submit-login', [AuthController::class, 'postLogin'])->name('login.post'); 
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('submit-registration', [AuthController::class, 'postRegistration'])->name('register.post'); 
Route::get('dashboard', [AuthController::class, 'dashboard']); 
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

//company
Route::get('companies', [App\Http\Controllers\CompanyController::class, 'index'])->name('company.index'); 
Route::get('create-company', [App\Http\Controllers\CompanyController::class, 'create'])->name('company.create'); 
Route::post('submit-company', [App\Http\Controllers\CompanyController::class, 'store'])->name('company.post'); 
Route::get('edit-company/{id}', [App\Http\Controllers\CompanyController::class, 'edit'])->name('company.edit');
Route::post('update-company', [App\Http\Controllers\CompanyController::class, 'update'])->name('company.update');
Route::get('delete-company/{id}', [App\Http\Controllers\CompanyController::class, 'destroy'])->name('company.delete'); 

//employee
Route::get('employees', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employee.index'); 
Route::get('create-employee', [App\Http\Controllers\EmployeeController::class, 'create'])->name('employee.create'); 
Route::post('submit-employee', [App\Http\Controllers\EmployeeController::class, 'store'])->name('employee.post'); 
Route::get('edit-employee/{id}', [App\Http\Controllers\EmployeeController::class, 'edit'])->name('employee.edit');
Route::post('update-employee', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employee.update');
Route::get('delete-employee/{id}', [App\Http\Controllers\EmployeeController::class, 'destroy'])->name('employee.delete'); 