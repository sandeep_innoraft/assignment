<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Employee;
class EmployeeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		//$employeeData = Employee::with('company')->get();
		$employeeData = Employee::with('company')->latest()->paginate(5);
		return view('employee.index',compact('employeeData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$companyList  = Company::select('id', 'name')->orderBy('id', 'desc')->pluck('name', 'id')->toArray();
		//echo "<pre>";print_r($companyList);die;
        return view('employee.create',compact('companyList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required'
        ]);
		
		//echo "<pre>";print_r($request->input());die;
		
		//validate email if not empty
		if(!empty($request->input('email'))) {
			$request->validate([
				'email'=>'email'
			]);
		}
		
		//validate phone if not empty
		if(!empty($request->input('phone'))) {
			$request->validate([
				'phone'=>'regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
			]);
		}
		
        Employee::create($request->all());
   
        return redirect()->route('employee.index')->with('success','Record has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$companyList  = Company::select('id', 'name')->orderBy('id', 'desc')->pluck('name', 'id')->toArray();
		
        $employeeData = Employee::find($id);
		//dd($userData);
        return view('employee.edit',compact('employeeData','id','companyList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
			'id' => 'required',
			'company_id' => 'required'
        ]);
		
		//validate email if not empty
		if(!empty($request->input('email'))) {
			$request->validate([
				'email'=>'email'
			]);
		}
		
		//validate phone if not empty
		if(!empty($request->input('phone'))) {
			$request->validate([
				'phone'=>'regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:12'
			]);
		}
		
		$data['company_id']	=   $request->company_id;        
		$data['first_name'] =   $request->first_name;        
		$data['last_name'] 	=   $request->last_name;
		$data['phone']   	=   $request->phone;
		$data['email']   	=   $request->email;
		Employee::where('id',$request->id)->update($data);
        return redirect()->route('employee.index')->with('success','Record has been updated successfully.');
    }
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::find($id)->delete();
  
        return redirect()->route('employee.index')->with('success','Record has been deleted successfully.');
    }
}
