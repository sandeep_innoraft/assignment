<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Storage;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        //$userData = Company::latest()->paginate(5);
        $companyData = Company::latest()->paginate(5);
		//dd($companyData);
		return view('company.index',compact('companyData'));
  
        //return view('userData.index',compact('userData'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
		
		//validate email if not empty
		if(!empty($request->input('email'))) {
			$request->validate([
				'email'=>'email'
			]);
		}
		
		//validate url if not empty
		if(!empty($request->input('website'))) {
			$request->validate([
				'website'=>'url'
			]);
		}
		
		
		//upload image
		$storage_location		=	NULL;
		if ($request->hasFile('logo')) {
			
			$request->validate([
                'logo' => 'mimes:jpeg,png,jpg,gif,svg|max:1024|dimensions:min_width=100,min_height=100',
            ]);
			
			 $filePath 			= 	$request->file('logo');
			 /* $fileName 			= 	'company_logo_'.time() . '.' . $filePath->getClientOriginalExtension();
			 $uploadedLogo 		=	 $request->file('logo')->storeAs('public/images/', $fileName);
			 $storage_location	=	$fileName; */
			 
			$imageName          =   'company_logo_'.time() . '.' . $filePath->getClientOriginalExtension();
            $request->logo->move(public_path('images'), $imageName);
            $storage_location	=   $imageName;
		 }
		//end upload image
		
		$requestData 			= $request->all();
		$requestData['logo'] 	= $storage_location;
        Company::create($requestData);
   
        return redirect()->route('company.index')->with('success','Record has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userData = Company::find($id);
        return view('company.show',compact('userData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userData = Company::find($id);
		//dd($userData);
        return view('company.edit',compact('userData','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$request->validate([
               'name' => 'required'
         ]);
		
		//validate email if not empty
		if(!empty($request->input('email'))) {
			$request->validate([
				'email'=>'email'
			]);
		}
		
		//validate url if not empty
		if(!empty($request->input('website'))) {
			$request->validate([
				'website'=>'url'
			]);
		}
		
		$data['name']       =   $request->name;        
		$data['email']  	=   $request->email;
		$data['website']   	=   $request->website;
		
		if ($request->hasFile('logo')) {
			$request->validate([
                'logo' => 'mimes:jpeg,png,jpg,gif,svg|max:1024|dimensions:min_width=100,max_height=100',
            ]);
			
			 $filePath 			= 	$request->file('logo');
			 /* $fileName 			= 	'company_logo_'.time() . '.' . $filePath->getClientOriginalExtension();
			 $uploadedLogo 		=	 $request->file('logo')->storeAs('public/images/', $fileName);
			 $storage_location	=	$fileName; */
			 
			$imageName          =   'company_logo_'.time() . '.' . $filePath->getClientOriginalExtension();
            $request->logo->move(public_path('images'), $imageName);
			$data['logo']   	=   $imageName;
		 }
		 
		Company::where('id',$request->id)->update($data);
  
        return redirect()->route('company.index')->with('success','Record has been updated successfully.');
    }
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::find($id)->delete();
  
        return redirect()->route('company.index')->with('success','Record has been deleted successfully.');
    }
}
